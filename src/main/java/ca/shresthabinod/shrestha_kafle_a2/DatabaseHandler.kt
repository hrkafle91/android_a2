package ca.shresthabinod.shrestha_kafle_a2

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHandler(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null,DATABASE_VERSION){
    companion object{
        private  val DATABASE_NAME = "MyDatabase"
        private  val DATABASE_VERSION = 1
        private  val TABLE_SOCCER = "SoccerTable"
        private  val KEY_ID = "id"
        private  val KEY_NAME = "PlayerName"
        private  val KEY_POSITION = "Position"
        private  val KEY_GOALS = "Goals"
    }

    override fun onCreate(db: SQLiteDatabase?) {
val CREATE_SOCCER_TABLE = ("CREATE TABLE " + TABLE_SOCCER + "(" + KEY_ID + "INTEGER PRIMARY KEY," + KEY_NAME + "TEXT,"+ KEY_POSITION + "TEXT,"+ KEY_GOALS + "INTEGER" + ")")

db?.execSQL(CREATE_SOCCER_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_SOCCER)
        onCreate(db)
    }

    //method to insert data
    fun addEmployee(sc: SoccerModelClass):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, sc.playerId)
        contentValues.put(KEY_NAME, sc.playerName)
        contentValues.put(KEY_POSITION,sc.position )
        contentValues.put(KEY_GOALS,sc.goals)
        // Inserting Row
        val success = db.insert(TABLE_SOCCER, null, contentValues)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }
    //method to read data
    fun viewPlayers():List<SoccerModelClass>{
        val sccList:ArrayList<SoccerModelClass> = ArrayList<SoccerModelClass>()
        val selectQuery = "SELECT  * FROM $TABLE_SOCCER"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var playerId: Int
        var playerName: String
        var position: String
        var goals: Int
        if (cursor.moveToFirst()) {
            do {
                playerId = cursor.getInt(cursor.getColumnIndex("playerId"))
                playerName = cursor.getString(cursor.getColumnIndex("playerName"))
                position = cursor.getString(cursor.getColumnIndex("position"))
                goals = cursor.getInt(cursor.getColumnIndex("goals"))
                val scc= SoccerModelClass(playerId = playerId, playerName = playerName, position = position, goals = goals)
                sccList.add(scc)
            } while (cursor.moveToNext())
        }
        return sccList
    }
    //method to update data
    fun updateEmployee(sc: SoccerModelClass):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, sc.playerId)
        contentValues.put(KEY_NAME, sc.playerName)
        contentValues.put(KEY_POSITION,sc.position )
        contentValues.put(KEY_GOALS,sc.goals)
        // Updating Row
        val success = db.update(TABLE_SOCCER, contentValues,"id="+sc.playerId,null)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }
    //method to delete data
    fun deletePlayer(sc: SoccerModelClass):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, sc.playerId) // SoccorModelClass UserId
        // Deleting Row
        val success = db.delete(TABLE_SOCCER,"id="+sc.playerId,null)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }
}
