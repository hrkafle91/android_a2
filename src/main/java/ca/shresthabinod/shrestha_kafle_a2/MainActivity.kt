package ca.shresthabinod.shrestha_kafle_a2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun savePlayers(view:View){

        val playerName = tfPlayerName.text.toString()
        val playerPosition = rbDefence.text.toString()
        val goals = tfNGoals.text.toString()

        val databaseHandler: DatabaseHandler= DatabaseHandler(this)
        if( playerName.trim()!="" && playerPosition.trim()!="" && goals.trim()!=""){
            val status = databaseHandler.addEmployee(SoccerModelClass(playerName,playerPosition,goals.toInt()))
            if(status > -1){
                Toast.makeText(applicationContext,"record saved",Toast.LENGTH_LONG).show()

                tfPlayerName.text.clear()
                tfNGoals.text.clear()
            }
        }else{
            Toast.makeText(applicationContext,"player name or goals cannot be blank",Toast.LENGTH_LONG).show()
        }

    }

    //method for read records from database in ListView
    fun viewRecord(view: View){
        //creating the instance of DatabaseHandler class
        val databaseHandler: DatabaseHandler= DatabaseHandler(this)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val scc: List<SoccerModelClass> = databaseHandler.viewPlayers()
        //val sccArrayId = Array<String>(scc.size){"0"}
        val sccArrayName = Array<String>(scc.size){"null"}
        val sccArrayPosition = Array<String>(scc.size){"null"}
        val sccArrayGoals = Array<Int>(scc.size){0}

        var index = 0
        for(e in scc){
          //  sccArrayId[index] = e.playerId.toString()
            sccArrayName[index] = e.playerName
            sccArrayPosition[index] = e.position
            sccArrayGoals[index] = e.goals
            index++
        }
        //creating custom ArrayAdapter
        val myListAdapter = MyListAdapter(this, sccArrayName, sccArrayPosition, sccArrayGoals)
        listView.adapter = myListAdapter
    }
}
