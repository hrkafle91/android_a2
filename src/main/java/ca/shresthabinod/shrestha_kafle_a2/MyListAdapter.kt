package ca.shresthabinod.shrestha_kafle_a2

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.TextView

class MyListAdapter(private val context: Activity, private val playerName: Array<String>, private val playerPosition: Array<String>, private val goals: Array<Int>)
    : ArrayAdapter<String>(context, R.layout.custom_list, playerName) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.custom_list, null, true)

        val player = rowView.findViewById<TextView>(R.id.tfPlayerName)
        val positions = rowView.findViewById<RadioButton>(R.id.tfPlayerPosition)
        val goal = rowView.findViewById<TextView>(R.id.tfGoals)

        player.text = "Player Name: ${playerName[position]}"
        positions.text = "Player Position: ${playerPosition[position]}"
        goal.text = "Goals: ${goals[position]}"

        return rowView
    }
}